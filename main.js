'use strict';

//let source = Rx.Observable.range(1, 5);

/**
 * Subscribe
 * onNext {cb} argument element from range
 * onError {cb} argument error
 * onCompleted {cb} argument undefined
 **/
/*
let subscription = source.subscribe(
  x => console.log(`next: ${x}`), // onNext
  e => console.log(`error: ${e}`), // onError
  () => console.log('complited') // onCompleted
);
*/

/*
let source = Rx.Observable.create(observer => {
  let i = 0;
  let inteval = setInterval(() => {
    if (i > 10) {
      clearInterval(inteval);
    }
    observer.onNext(i);
    console.log('inteval: ', i);
    i++;
  }, 1000);
  setTimeout(() => {
    observer.onCompleted();
  }, 10000);
  return () => console.log('test');
});

let subscription = source.subscribe(
  x => console.log(`next: ${x}`), // onNext
  e => console.log(`error: ${e}`), // onError
  () => console.log('complited') // onCompleted
);
*/

/**
 * dispose
 * Fulfills cb which return in "Rx.Observable.create" cb
 **/
//setTimeout(() => {
  //subscription.dispose();
//}, 5000);

var observer = Rx.Observer.create(
  x => console.log('onNext: ', x),
  e => console.log('onError: %s', e),
  () => console.log('onCompleted')
);

Rx.Observable.timer(
  5000,
  1000
)
  .timestamp()
  .subscribe(
    observer
  );

